package com.syiaas.springcloud.cfgbean;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class ConfigBean {//spring  applicationContext.xml 使用springboot @Configuration替换
	
    @Bean
    public RestTemplate getRestTemplate(){
		return new RestTemplate();
    }
    
}
