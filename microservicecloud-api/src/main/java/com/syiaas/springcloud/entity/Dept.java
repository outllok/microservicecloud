package com.syiaas.springcloud.entity;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@AllArgsConstructor
@NoArgsConstructor
//@Data注解在类上，会为类的所有属性自动生成setter/getter、equals、canEqual、hashCode、toString方法
@Data
@Accessors(chain=true)
public class Dept implements Serializable{
    private Long deptno;//主键
    private String dname;//部门名称
    private String db_source;//来自那个数据库；因为微服务架构可以一个服务对应一个数据库
    
    public static void main(String[] args) {
		Dept dept = new Dept();
		// 链式写法
		dept.setDeptno(2L).setDname("JAVA").setDb_source("one");
		System.out.println(dept);
	}
    
}
